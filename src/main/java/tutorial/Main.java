package tutorial;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

//import java.security.NoSuchAlgorithmException;
//import java.security.spec.InvalidKeySpecException;
//
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import org.bouncycastle.util.io.pem.PemObject;
//import org.bouncycastle.util.io.pem.PemReader;
//
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.security.KeyFactory;
//import java.security.PrivateKey;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.time.Instant;
//import java.util.Date;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) throws TelegramApiException {
        // создаем экземпляр бота и запускаем
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        botsApi.registerBot(new Bot());

        /** Получение IAM-токена для сервисного аккаунта
         *  Создать JWT (код ниже реализация)
         *  TODO потом Обменять JWT на IAM-токен (шаблон API ниже)
         *  шаблон API
         *  curl -X POST \
         *     -H 'Content-Type: application/json' \
         *     -d '{"jwt": "<JWT-токен>"}' \
         *     https://iam.api.cloud.yandex.net/iam/v1/tokens
         */
//        PemObject privateKeyPem;
//
//        try (PemReader reader = new PemReader(new FileReader(System.getProperty("user.dir") + "/src/main/resources/authorized_key.json"))) {
//            privateKeyPem = reader.readPemObject();
//            System.out.println("privateKeyPem.getContent() "+ privateKeyPem.getContent());
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyPem.getContent()));
//
//        String serviceAccountId = "ajemq91f05gmepimb9t9";
//        String keyId = "AQVN2awpbbRLfi7IJJf5dwVLbv715MllmM17ZMAI";
//
//        Instant now = Instant.now();
//
//        // Формирование JWT.
//        String encodedToken = Jwts.builder()
//                .setHeaderParam("kid", keyId)
//                .setIssuer(serviceAccountId)
//                .setAudience("https://iam.api.cloud.yandex.net/iam/v1/tokens")
//                .setIssuedAt(Date.from(now))
//                .setExpiration(Date.from(now.plusSeconds(360)))
//                .signWith(privateKey, SignatureAlgorithm.PS256)
//                .compact();
//
//        System.out.println("encodedToken " + encodedToken);
    }
}