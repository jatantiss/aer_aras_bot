package tutorial;

import io.restassured.RestAssured;
import io.restassured.response.Response;

class Weather {
    private String url;
    private String city;

    public void setCity(String city) {
        this.city = city;
        this.url = "https://api.openweathermap.org/data/2.5/weather?q="+ city +"&units=metric&lang=ru&appid=79d1ca96933b0328e1c7e3e7a26cb347";
    }

    public String getCity(){
        return city;
    }

    // получаем погоду по API
    public String getWeather() {

        Response responseWorkspaces = RestAssured
                .given()
                .contentType("application/json")
                .when()
                .get(url);

        String description = String.valueOf((responseWorkspaces.body().jsonPath().getString("weather.description")));
        String temp = String.valueOf((responseWorkspaces.body().jsonPath().getString("main.temp")));
        String feels_like = String.valueOf((responseWorkspaces.body().jsonPath().getString("main.feels_like")));
        return "Сегодня " + description + ", температура: " + temp + ", ошущается как: " + feels_like;
    }
}
