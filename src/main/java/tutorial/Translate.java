package tutorial;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Translate {

    //подключение АПИ переводчика
    public String getTranslate(String data) {
        Response responseWorkspaces = RestAssured
                .given()
                .header("Authorization", "Bearer " + ProjectSettings.TOKEN)
                .body(data)
                .when()
                .post(ProjectSettings.URL_TRANSLATE_API);

        return String.valueOf((responseWorkspaces.body().jsonPath().getString("translations.text")));
    }
}
