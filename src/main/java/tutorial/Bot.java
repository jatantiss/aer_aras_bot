package tutorial;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
//import org.telegram.telegrambots.meta.api.methods.CopyMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;


public class Bot extends TelegramLongPollingBot {
    private boolean translate = false;
    private boolean weather = false;

    @Override
    public String getBotUsername() {
        return ProjectSettings.BOT_USER_NAME;
    }

    @Override
    public String getBotToken() {
        return ProjectSettings.BOT_TOKEN;
    }

    @Override
    public void onUpdateReceived(Update update) {
        var msg = update.getMessage();
        var user = msg.getFrom();
        var userId = user.getId();
        String VRN = "Воронеж";

        Weather vrnWeather = new Weather();
        Translate textTranslate = new Translate();

        // ловим в консоле введенное сообщение в чат бота
        System.out.println(user.getFirstName() + " wrote " + msg.getText() + userId);

        // переключатель между меню
        if(msg.isCommand()){
            if(msg.getText().equals("/translate")) {
                translate = true;
                weather = false;
            }
            else if (msg.getText().equals("/weather"))
            {
                weather = true;
                translate = false;
                // передаем город Воронеж и выводим форматированный вывод в чат
                vrnWeather.setCity(VRN);
                sendText(userId, vrnWeather.getWeather());
            }

        }

        if (weather) {
            System.out.println(vrnWeather.getWeather());
        } else if (translate){
            // переводим текст с англ на ру и выводим в консоль объект который отправляем на бек
            sendText(userId, textTranslate.getTranslate(data(ProjectSettings.FOLDER_ID, msg.getText(), ProjectSettings.TARGET_LANGUAGE_CODE)));
            System.out.println(data(ProjectSettings.FOLDER_ID, msg.getText(), ProjectSettings.TARGET_LANGUAGE_CODE));
        }


    }

    public void sendText(Long who, String what){
        SendMessage sm = SendMessage.builder()
                .chatId(who.toString())
                .text(what).build();
        try {
            execute(sm);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }

//    public void copyMessage(Long who, Integer msgId){
//        CopyMessage cm = CopyMessage.builder()
//                .fromChatId(who.toString())  //We copy from the user
//                .chatId(who.toString())      //And send it back to him
//                .messageId(msgId)            //Specifying what message
//                .build();
//        try {
//            execute(cm);
//        } catch (TelegramApiException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public String data(String folderId, String texts, String targetLanguageCode) {
        return "{\n" +
                "    \"folderId\":  \"" + folderId + "\",\n" +
                "    \"texts\":  [\"" + texts + "\"],\n" +
                "    \"targetLanguageCode\":  \"" + targetLanguageCode + "\"\n" +
                "}";
    }

}